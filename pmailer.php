<?php

use PHPMailer\PHPMailer;

require './phpmailer/PHPMailer.php';
require './phpmailer/class.smtp.php';

/**
 * 如果是用 mailcatcher 作为本地开发测试,
 * 则 SMTPAuth, SMTPSecure 注释掉
 * Host 设置为 127.0.0.1。如果是用docker, 则设置为 mailcatcher 容器的IP.
 * Port 设置为 1025
 */

$mailer = new PHPMailer(true);                              // Passing `true` enables exceptions

//服务器配置
$mailer->CharSet = "UTF-8";          //设定邮件编码
$mailer->SMTPDebug = 0;             // 调试模式输出
$mailer->isSMTP();                  // 使用SMTP
$mailer->SMTPAuth = true;           // 允许 SMTP 认证
$mailer->SMTPSecure = 'ssl';        // 允许 TLS 或者ssl协议
$mailer->Host = 'smtp.gmail.com';   // SMTP服务器
$mailer->Username = '';             // SMTP 用户名.即邮箱的用户名
$mailer->Password = '';             // SMTP 密码. 部分邮箱是授权码(例如163邮箱)
$mailer->Port = 465;                // 服务器端口 25 或者465 具体要看邮箱服务器支持

$mailer->setFrom('', '');  //发件人
$mailer->addAddress('', '');  // 收件人
//$mailer->addAddress('ellen@example.com');  // 可添加多个收件人
$mailer->addReplyTo('', ''); //回复的时候回复给哪个邮箱 建议和发件人一致
//$mailer->addCC('cc@example.com'); //抄送
//$mailer->addBCC('bcc@example.com');   //密送

//发送附件
// $mailer->addAttachment('../xy.zip'); // 添加附件
// $mailer->addAttachment('../thumb-1.jpg', 'new.jpg'); // 发送附件并且重命名

//Content
$mailer->isHTML(true);  // 是否以HTML文档格式发送  发送后客户端可直接显示对应HTML内容
$mailer->Subject = '这里是邮件标题' . date('Y-m-d H:i:s');
$mailer->Body    = '<h1>这里是邮件内容</h1>';
$mailer->AltBody = '如果邮件客户端不支持HTML则显示此内容';

try {
    $mailer->send();
    echo '邮件发送成功';
} catch (\Exception $e) {
    echo '邮件发送失败: ', $mailer->ErrorInfo;
}

echo "\r\n";
